#!/usr/bin/env python3
import datetime
import hashlib
import multiprocessing
import random
import subprocess
import time
from multiprocessing import cpu_count
from multiprocessing.queues import Queue


def _cook(commit_file: bytes, result_queue: Queue):
    append_length = 2 + 20
    length = len(commit_file) + append_length
    prefix = b'commit ' + str(length).encode() + b'\0' + commit_file + b'\n'
    prehash = hashlib.sha1(prefix)
    best_beefscore = 0

    base = random.randint(0, 100000000000000000000)
    i = 0
    while True:
        i += 1
        hash = prehash.copy()
        append = str(base + i).rjust(20, '0') + '\n'
        hash.update(append.encode())

        digest = hash.hexdigest()
        if not digest.startswith('be'):
            continue
        beef = True
        for beefindex in range(2, len(digest)):
            if digest[beefindex] == 'e':
                continue
            if digest[beefindex] == 'f':
                break
            beef = False
            break
        if beef and beefindex > best_beefscore:
            result_queue.put_nowait((i, digest, beefindex, append))
            best_beefscore = beefindex
    return append, hash


def main():
    commit = subprocess.check_output('git rev-parse HEAD').decode().strip()
    commit_file = subprocess.check_output(f'git cat-file commit {commit}')

    epoch = subprocess.check_output('git show -s --format=%ct HEAD').decode().strip()
    print(f'set GIT_COMMITTER_DATE={epoch}')

    start = datetime.datetime.now()

    ctx = multiprocessing.get_context('spawn')
    result_queue = ctx.Queue()
    workers = [ctx.Process(target=_cook, args=(commit_file, result_queue)) for _ in range(cpu_count())]
    best_beefindex = 0
    try:
        for worker in workers:
            worker.start()

        while True:
            iteration, digest, beefindex, append = result_queue.get()
            if beefindex > best_beefindex:
                best_beefindex = beefindex
                print(datetime.datetime.now() - start, iteration)
                print(digest, beefindex, append)
                # subprocess.check_call(['git', 'commit', '--no-verify', '--amend', '-m', commit_content + append],
                #                       env={**os.environ, 'GIT_COMMITTER_DATE': str(epoch)}, shell=False)
        while True:
            time.sleep(120)
    finally:
        for worker in workers:
            worker.kill()


if __name__ == '__main__':
    main()
